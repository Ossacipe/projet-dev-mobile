import React from 'react';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import AppNavigator from './src/navigation/Navigation';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Store, Persistor } from './src/store/config';
import Toast from 'react-native-toast-message';

function HomeScreen() {
  return (
    <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <AppNavigator/>
        </PersistGate>
      </Provider>
  );
}

export default function App() {
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <HomeScreen/>
      </ApplicationProvider>
      <Toast position='bottom'/>
    </>
  );
}