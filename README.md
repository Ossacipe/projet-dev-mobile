# Projet Développement Mobile

## Compositon du groupe
- Mielnikoff Théo
- Penz Théophile

## Fonctionalités supplémentaires
- Ajout des types de lieux utilsés dans les API de Google (dans un fichier JSON comprenant une valeur et un libellé)
- Ajout d'un nouvel écran pour effectuer une recherche de lieux avec l'API de Google (Place Search - Text Search : https://developers.google.com/maps/documentation/places/web-service/search-text)
- Lors d'un clic sur un lieu (dans l'écran Google), ajout dans la liste de lieux, en récupérant les détails du lieu avec l'API Google (Place Details : https://developers.google.com/maps/documentation/places/web-service/details)
- Les lieux Google possédent des informations supplémentaire dans leur affichage (Numéro de téléphone, note, horaires d'ouvertures, site internet)
- Impossibilité de modifier un lieu Google, à la place on propose une actualisation des données du lieu

## Technologies employées
- UI Kitten pour l'interface
- Expo Location pour le geocoding, le reverse geocoding et la géolocalisation
- Geolib pour les calculs géogaphiques (Est-ce qu'on point est inclus dans un rayon)
- react-native-toast-message pour l'affichage de Toast Message
