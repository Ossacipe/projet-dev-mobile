import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomNavigation, BottomNavigationTab } from '@ui-kitten/components';
import Search from '../views/Search';
import Places from '../views/Places';
import PlaceDetails from '../views/PlaceDetails';
import GooglePlaces from '../views/GooglePlaces';
import AddPlace from '../views/AddPlace';
import {PlacesIcon, SearchIcon, GoogleIcon} from '../definitions/Icones'

const PlacesNavigation = createStackNavigator();
const SearchNavigation = createStackNavigator();
const TabNavigation = createBottomTabNavigator();

/*
  La navigation sur l'écran Search (avec un StackNavigator)
*/
function SearchScreen() {
  return (
    <SearchNavigation.Navigator
      initialRouteName="ViewSearch" screenOptions={{headerShown: false}}>

      <SearchNavigation.Screen
        name="ViewSearch"
        component={Search}/>

      <SearchNavigation.Screen
        name="ViewAddPlace"
        component={AddPlace}/>

      <SearchNavigation.Screen
        name="ViewPlaceDetails"
        component={PlaceDetails}/>
        
    </SearchNavigation.Navigator>
  )
}

/*
  La navigation sur l'écran Places (avec un StackNavigator)
*/
function PlacesScreen() {
  return (
    <PlacesNavigation.Navigator
      initialRouteName="ViewPlaces" screenOptions={{headerShown: false}}>

      <PlacesNavigation.Screen
        name="ViewPlaces"
        component={Places}/>

      <PlacesNavigation.Screen
        name="ViewPlaceDetails"
        component={PlaceDetails}/>

      <PlacesNavigation.Screen
        name="ViewAddPlace"
        component={AddPlace}/>

    </PlacesNavigation.Navigator>
  )
}

/*
  Rendu de la navigation
*/
const BottomTabBar = ({ navigation, state }) => (
  <BottomNavigation
    selectedIndex={state.index}
    onSelect={index => navigation.navigate(state.routeNames[index])}>
    <BottomNavigationTab title='Lieux' icon={(PlacesIcon)}/>
    <BottomNavigationTab title='Google' icon={(GoogleIcon)}/>
    <BottomNavigationTab title='Recherche' icon={(SearchIcon)}/>
  </BottomNavigation>
)

/*
  Tab Navigator de l'application
*/
function TabNavigator() {
  return (
    <TabNavigation.Navigator tabBar={props => <BottomTabBar {...props} />}  screenOptions={{headerShown: false}}>
      <TabNavigation.Screen name='Places' component={PlacesScreen}/>
      <TabNavigation.Screen name='GooglePlaces' component={GooglePlaces}/>
      <TabNavigation.Screen name='Search' component={SearchScreen}/>
    </TabNavigation.Navigator>
  )
}

/*
  Navigation
*/
function AppNavigator() {
  return (
    <NavigationContainer>
      <TabNavigator/>
    </NavigationContainer>
  );
}

export default AppNavigator;