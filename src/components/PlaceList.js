import React from 'react';
import { Divider, List, ListItem } from '@ui-kitten/components';
import { EyeIcon } from '../definitions/Icones';

const PlaceList = ({onClickOnPlace, lstPlace}) => {
    
  const renderItem = ({ item }) => (
     <ListItem
       title={`${item.name}`}
       accessoryLeft={EyeIcon}
       description={`${item.adress.name}, ${item.adress.postalCode} ${item.adress.city}`}
       onPress={() => { onClickOnPlace(item) }}
     />
  );
  
  return (
    <List
      data={lstPlace}
      ItemSeparatorComponent={Divider}
      renderItem={renderItem}/>);

};

export default PlaceList;
