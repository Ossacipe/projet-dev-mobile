import React from 'react';
import { IndexPath, Select, SelectItem } from '@ui-kitten/components';
import { TagIcon } from '../definitions/Icones';

const data = require('../services/PlacesType.json').sort((a,b) => a.libelle.localeCompare(b.libelle));

const SelectTags = ({initialData, selectChange, status}) => {
  
  const [selectedTags, setSelectedTags] = React.useState([]);

  // Retourne les valeurs des tags sélectionnés
  const selectedTagsValues = selectedTags.map(index => {
      return data[index.row].value;
  });

  // Retourne les libellés des tags sélectionnés
  const selectedTagsLibelles = selectedTags.map(index => {
    return data[index.row].libelle;
  });

  // Rendu d'un item de la liste
  const renderOption = (type) => (
    <SelectItem key={type.value} title={type.libelle}/>
  );

  // Transimission des nouvelles valeurs en cas de changement
  React.useEffect(() => {
    selectChange(selectedTagsValues);
  }, [selectedTags]);

  // Au chargement du composant, on récupére les valeurs sélectionnées si elles existent
  React.useEffect(() => {
    if(initialData){
      var lstSelected = [];
      initialData.forEach(value => {
        var index = data.findIndex(type => type.value == value);
        lstSelected.push(new IndexPath(index));
      });
      setSelectedTags(lstSelected);
    }
  }, []);

  return (
    <Select
          status={status}
          accessoryLeft={TagIcon}
          placeholder='Tags'
          multiSelect={true}
          selectedIndex={selectedTags}
          style={{marginTop:10}}
          value={selectedTagsLibelles.join(', ')}
          onSelect={index => setSelectedTags(index)}>
          {data.map(renderOption)}
        </Select>
  );
};

export default SelectTags;
