import React from 'react';
import { StyleSheet, View} from 'react-native';
import { Marker } from 'react-native-maps';

const MarkerList = ({favoris}) => {
    return  (
        <Marker coordinate={{latitude: favoris.position.latitude,longitude: favoris.position.longitude,latitudeDelta: 0.04,longitudeDelta: 0.05,}} title={favoris.name} description={favoris.description} anchor={{x: 0.5, y: 0.5}}>
        <View style={styles.circle}>
        <View style={styles.stroke}/>
        <View style={styles.core}/>
        </View>
        </Marker> );
};

//Style
const styles = StyleSheet.create({
    circle :{
      width : 16,
      height:16,
      borderRadius:50,
      shadowColor: '#555',
      shadowOffset:{
        width : 2,
        height : 2
      },
      shadowOpacity : 0.9
    },
    stroke: {
      width : 16,
      height:16,
      borderRadius:50,
      backgroundColor: "#fff",
      zIndex: 1
    },
    core :{
      width : 12,
      height : 12,
      position:"absolute",
      top:1,
      left:1,
      right : 1,
      bottom : 1,
      backgroundColor: "red",
      zIndex:2,
      borderRadius:50
    }
  });

export default MarkerList;
