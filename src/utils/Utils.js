import * as Location from 'expo-location';
import { getDistance, isPointWithinRadius, isPointInPolygon } from 'geolib';
const data = require('../services/PlacesType.json');

// Retourne les libellés des type à partir de leurs valeurs
export const displayPlaceType = (lstType) => {
    return lstType.map(type => {
        return data.find(elem => elem.value == type).libelle;
    }).join(', ');
}

// Retourne la position actuelle de l'utilisateur
export async function getCurrentLocation() {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
        throw new Error("Pas d'autorisation");
    }
    let location = await Location.getCurrentPositionAsync({});
    let userLatitude = parseFloat(JSON.stringify(location.coords.latitude));
    let userLongitude = parseFloat(JSON.stringify(location.coords.longitude));
    return {latitude: userLatitude, longitude: userLongitude};
}

// Retourne la latitude et longitude associée à une adresse
export async function geocoding(adresse) {
    return Location.geocodeAsync(adresse);
}

// Retourne l'adresse associée à une latitude et longitude
export async function reverseGeocoding(position) {
    return Location.reverseGeocodeAsync(position);
}

// Retourne la distance entre deux points
export function distanceBeteween2Points(point1, point2){
    return getDistance(point1, point2);
}

// Dit si un point est inclus dans un rayon
export function isPointInRadius(point, center, radius){
    return isPointWithinRadius(point,center,radius)
}

export function isInPolygon(point, boundariesPoints){
    return isPointInPolygon(point, boundariesPoints)
}
