import React from 'react';
import { Icon } from '@ui-kitten/components';

export const PlacesIcon = (props) => (
    <Icon {...props} name='pin-outline'/>
)
  
export const SearchIcon = (props) => (
    <Icon {...props} name='search-outline'/>
)

export const BackIcon = (props) => (
    <Icon {...props} name='arrow-back' />
);

export const PlusIcon = (props) => (
    <Icon {...props} name='plus-outline' />
);

export const PinIcon = (props) => (
    <Icon {...props} name='pin-outline' />
);

export const TagIcon = (props) => (
    <Icon {...props} name='pricetags-outline' />
);

export const CloseIcon = (props) => (
    <Icon {...props} name='close-outline' />
);

export const GoogleIcon = (props) => (
    <Icon {...props} name='google-outline' />
);

export const NavigationIcon = (props) => (
    <Icon {...props} name='navigation-2-outline' />
);

export const EditIcon = (props) => (
    <Icon {...props} name='edit-outline' />
);

export const CalendarIcon = (props) => (
    <Icon {...props} name='calendar-outline' />
);

export const StarIcon = (props) => (
    <Icon {...props} name='star-outline' />
);

export const PhoneCallIcon = (props) => (
    <Icon {...props} name='phone-call-outline' />
);

export const LinkIcon = (props) => (
    <Icon {...props} name='external-link-outline' />
);

export const BookIcon = (props) => (
    <Icon {...props} name='book-open-outline' />
);

export const DownloadIcon = (props) => (
    <Icon {...props} name='cloud-download-outline' />
);

export const EyeIcon = (props) => (
    <Icon {...props} name='eye-outline' />
);

export const PlusCircleIcon = (props) => (
    <Icon {...props} name='plus-circle-outline' />
);


