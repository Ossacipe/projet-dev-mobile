import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, SafeAreaView ,View} from 'react-native';
import { connect } from 'react-redux';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Divider, TopNavigation,Layout, Text, TopNavigationAction, Spinner } from '@ui-kitten/components';
import {PlusIcon} from '../definitions/Icones'
import { getCurrentLocation, isInPolygon } from '../utils/Utils';
import { Toast } from 'react-native-toast-message/lib/src/Toast';
import MarkerList from '../components/MarkerList';
import PlaceList from '../components/PlaceList';


const Places = ({ navigation, favoris }) => {

  const [isLoading, setIsLoading] = useState(true);
  const [currentRegion, setCurrentRegion] = useState(null);
  const [lstPlace, setLstPlace] = useState([]); // Liste des lieux affichés
  const mapRef = useRef(null);

  // Navigation vers les détails d'un lieu
  const navigateToPlaceDetails = (place) => {
    navigation.navigate("ViewPlaceDetails", { datePlace: place.addDate });
  };

  // Quand la liste de favoris change
  useEffect(() => {
    findLocation();
    if(mapRef && mapRef.current){
      resetList();
    }
  }, [favoris]);

  // Filtre la liste
  const resetList = () => {
    mapRef.current.getMapBoundaries().then((boundaries) => {

      //Create Boundaries NorthWest
      var latitude  = boundaries.northEast.latitude;
      var longitude = boundaries.southWest.longitude;
      var northWest={latitude,longitude};

      //Create Boundaries southEast
      latitude  = boundaries.northEast.longitude;
      longitude = boundaries.southWest.latitude;
      var southEast={latitude,longitude};

      const BoundariesPoints = [boundaries.northEast,northWest,boundaries.southWest, southEast]
      const list = favoris.filter((e) => isInPolygon(e.position,BoundariesPoints));
      setLstPlace(list);
    });
  };

  // Recherche de la position actuelle de l'utilisateur
  const findLocation = async () => {
    setIsLoading(true);
    try {
      const res = await getCurrentLocation();
      const newRegion = {
        latitude: res.latitude,
        longitude: res.longitude,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05,
      }
      setCurrentRegion(newRegion);
    } catch (error) {
      Toast.show({
        type: 'error',
        text1: error,
      });
    }
    setIsLoading(false);
  }

  const navigateAdd = () => {
    navigation.navigate('ViewAddPlace');
  };

  const AddAction = () => (
    <TopNavigationAction icon={PlusIcon} onPress={navigateAdd}/>
  );

  const regionChange = (region) => {
    setCurrentRegion(region);
    resetList();
  }

  return (
      <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title='Lieux' alignment='center' accessoryRight={AddAction}/>
      <Divider/>
      <View style={styles.container}>
      {isLoading ?
      (<Spinner/>) :
      (<MapView
        provider={PROVIDER_GOOGLE}
        style={styles.MapStyle}
        customMapStyle={mapStyle}
        ref={mapRef}
        region={currentRegion}
        showsUserLocation={true}
        onRegionChangeComplete={regionChange}
      > 
      {favoris ? favoris.map((place, index) => (<MarkerList key={index} favoris={place}/>)):null}
      </MapView>)}
      <Text style={styles.GapStyle} />
      <SafeAreaView style={styles.ListStyle}>
        <Layout style={styles.ListStyle}>
          <PlaceList onClickOnPlace={navigateToPlaceDetails} lstPlace={lstPlace}></PlaceList>
        </Layout>
      </SafeAreaView>
    </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    favoris: state.lstFav
  }
}

var mapStyle = [
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

// Other style
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  MapStyle : {
    flex: 20,
  },
  ListStyle : {
    flex: 15,
  },
  GapStyle : {
    flex:1,
    backgroundColor : 'white'
  }
});

export default connect(mapStateToProps)(Places);