import React, { useState } from 'react';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-native';
import { Divider, Layout, TopNavigation, Input, Button, Select, SelectItem, IndexPath, List, ListItem, Spinner, Text} from '@ui-kitten/components';
import { PlusCircleIcon, SearchIcon, StarIcon, TagIcon } from '../definitions/Icones';
import Toast from 'react-native-toast-message';
import { findPlace, getPlaceDetails } from '../services/Google';
import { geocoding, reverseGeocoding } from '../utils/Utils';

const data = require('../services/PlacesType.json').sort((a,b) => a.libelle.localeCompare(b.libelle));

const GooglePlaces = ({ navigation, route, favoris, dispatch }) => {

    const [selectedTag, setSelectedTag] = useState(new IndexPath(0)); // Tag sélectionné
    const [search, setSearch] = useState(""); // Critére de recherche
    const [lstRes, setLstRes] = useState([]); // La liste résultat
    const [loading, setLoading] = useState(false); // En chargement ?

    // Recherche
    const recherche = async() => {
        setLoading(true);
        try {
            // Requête de recherche
            const res = await findPlace(search, displayTag.value);
            setLstRes(res.results);
        }
        catch(error){
            Toast.show({
              type: 'error',
              text1: error,
            });
        }
        setLoading(false);
    }

    // Affiche le tag actuellement sélectionnée
    const displayTag = data[selectedTag.row];

    // Rendu du tag dans le select
    const renderOption = (elem, index) => (<SelectItem key={index} title={elem.libelle}/>);

    // Rendu de la partie droite d'un item de la liste
    const renderItemRight = (props, item) => (
        <Layout style={{flexDirection: 'row', alignItems: 'center'}}>
            <StarIcon fill='#000000' style={{width: 20, height: 20}}></StarIcon>
            <Text>{item.rating}</Text>
        </Layout>
    )

    // Rendu d'un item de la liste
    const renderItem = ({item, index}) => ( 
        <ListItem
            title={item.name}
            description={item.formatted_address}
            accessoryLeft={PlusCircleIcon}
            accessoryRight={props => renderItemRight(props, item)} 
            onPress={() => { save(item) }}/>
    );

    // Sauvegarde du lieu sélectionné
    const save = async(item) => {

        const found = favoris.find(place => place.place_id == item.place_id);

        if(found){
            Toast.show({
                type: 'error',
                text1: 'Ce lieu est déjà dans votre liste !',
            });
        }
        else {
            try {

                // Récupération du lieu détaillé
                var itemDetails = await getPlaceDetails(item.place_id);

                // On geocode l'adresse du lieu
                var position = await geocoding(itemDetails.result.formatted_address);

                // On récupére l'adresse bien formatée
                var fullAdresse = await reverseGeocoding(position[0]);

                // Ajout des propriétés spécifiques à notre application dans l'objet
                var object = {adress: fullAdresse[0], position: position[0], addDate: new Date().toJSON()};

                // Ajout
                var action = { type: 'ADD', value: Object.assign(itemDetails.result,object)};
                dispatch(action);

                Toast.show({
                    type: 'success',
                    text1: 'Lieu ajouté !',
                });
            }
            catch(error){
                Toast.show({
                  type: 'error',
                  text1: error,
                });
            }
        }
    }

    // Icone de chargement
    const LoadingIndicator = (props) => (
        <Spinner size='small'/>
    );

    return (
        <SafeAreaView style={{ flex: 1 }}>
        <TopNavigation title='Lieux Google' alignment='center'/>
        <Divider/>
        <Layout style={{padding: 25 }}>
            
            <Input
                placeholder='Recherche'
                value={search}
                onChangeText={nextValue => setSearch(nextValue)}
                accessoryLeft={SearchIcon}/>

            <Select
                style={{marginTop: 10}}
                placeholder='Tags'
                value={displayTag.libelle}
                accessoryLeft={TagIcon}
                selectedIndex={selectedTag}
                onSelect={index => setSelectedTag(index)}>
                {data.map(renderOption)}
            </Select>

            {loading ? (
                <Button status='primary' accessoryLeft={LoadingIndicator} style={{marginTop:10}} disabled={true}>
                    Chargement
                </Button>
            ) :
            (
                <Button status='primary' accessoryLeft={SearchIcon} style={{marginTop:10}} onPress={recherche}>
                    Recherche
                </Button>
            )}

            <Divider/>

        </Layout>

        <Layout style={{ flex: 1, justifyContent: 'center' }}>

            <List
                data={lstRes}
                ItemSeparatorComponent={Divider}
                renderItem={renderItem}/>
            
        </Layout>
        </SafeAreaView>
    );
};

const mapStateToProps = (state) => {
  return {
    favoris: state.lstFav
  }
}

export default connect(mapStateToProps)(GooglePlaces);