import React, { useState } from 'react';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-native';
import { Divider, Layout, TopNavigation, TopNavigationAction, Input, Button, Spinner } from '@ui-kitten/components';
import { BackIcon, PlusIcon, PinIcon, NavigationIcon, EditIcon } from '../definitions/Icones';
import SelectTags from '../components/SelectTags';
import Toast from 'react-native-toast-message';
import { geocoding, getCurrentLocation, reverseGeocoding } from '../utils/Utils';

const AddPlace = ({ navigation, route, favoris, dispatch }) => {

  // Gestion du retour
  const navigateBack = () => {
    navigation.goBack();
  };

  // Icone retour à l'écran suivant
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack}/>
  );

  // Icone de chargement
  const LoadingIndicator = (props) => (
      <Spinner size='small'/>
  );

  const editMode = (route.params && route.params.place);
  const [nom, setNom] = useState(editMode ? route.params.place.name : '');
  const [description, setDescription] = useState(editMode ? route.params.place.description : '');
  const [adresse, setAdresse] = useState(editMode ? route.params.place.adress.name + " " + route.params.place.adress.postalCode + " " + route.params.place.adress.city : '');
  const [selectedTags, setSelectedTags] = useState(editMode ? route.params.place.types : []);
  const [saveForm, setSaveForm] = useState(false);
  const [usePosition, setUsePosition] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  // Génération du bouton de sauvegarde
  const ButtonSave = () => {
    var color = "";
    var icon = "";
    var text = "";

    if(editMode){
      color = "primary";
      icon = EditIcon;
      text = "Modifier";
    }
    else {
      color = "success";
      icon = PlusIcon;
      text = "Ajouter";
    }
    
    if(isLoading){
      return (
      <Button status={color} accessoryLeft={LoadingIndicator} style={{marginTop:25}} disabled={true}>
        Chargement
      </Button>);
    }
    else {
      return (
      <Button status={color} accessoryLeft={icon} style={{marginTop:25}} onPress={save}>
        {text}
      </Button>);
    }
  }

  // Fonction pour valider et enregistrer un lieu
  const save = async () => {
    setIsLoading(true)
    setSaveForm(true);

    let fullAdresse = null;
    let position = null;

    try {

      // Si mode géolocalisation activé : on récupére la position actuelle
      if(usePosition){
        position = await getCurrentLocation();
      }
      // Sinon : on essaye le geocoding sur l'adresse saisie
      else {
        let res = await geocoding(adresse);
        position = res[0];
      }

      // On récupére un objet adresse bien formaté
      fullAdresse = await reverseGeocoding(position);

      // Si les données sont bien saisies
      if(nom!="" && description!="" && fullAdresse && selectedTags.length > 0){

        var action;

        // En mode édition : on fait un UPDATE du lieu
        if(editMode){
          var data = {name : nom, description: description, types: selectedTags, adress: fullAdresse[0], position: position};
          action = { type: 'UPDATE', value: Object.assign(route.params.place,data)};
        }
        // Sinon : on est en mode création, on fait un ADD
        else {
          action = { type: 'ADD', value: {name : nom, description: description, types: selectedTags, adress: fullAdresse[0], position: position, addDate: new Date().toJSON()} };
        }

        dispatch(action);
        navigation.goBack();

        Toast.show({
          type: 'success',
          text1: editMode ? 'Lieu modifié !' : 'Lieu ajouté !',
        });
      }
      // Sinon : les données sont mal saisies : erreur
      else {
        Toast.show({
          type: 'error',
          text1: editMode ? 'Impossible de modifier le lieu !' : 'Impossible d\'ajouter le lieu !',
        });
      }
    }
    catch(error){
      Toast.show({
        type: 'error',
        text1: error,
      });
    }
    setIsLoading(false);
  }

  // Fonction pour activer / désactiver le mode localisation
  const toggleLocalisation = () => {
    Toast.show({
      type: 'info',
      text1: usePosition ? 'Mode position actuelle désactivé.' : 'L\'adresse utilisée sera votre position actuelle.',
    });
    setUsePosition(!usePosition);
  }

  // Détermine la couleur d'un champ en fonction de la validité de la valeur
  const inputColor = (val) => {
    return (saveForm && val == "" ? 'danger' : 'basic')
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title={editMode ? 'Modification' : 'Ajout d\'un lieu'} alignment='center' accessoryLeft={BackAction}/>
      <Divider/>
      <Layout style={{ flex: 1, padding: 25 }}>
        <Input
          status={inputColor(nom)}
          placeholder='Nom'
          value={nom}
          onChangeText={nextValue => setNom(nextValue)}/>
          
        <Input
          status={inputColor(description)}
          placeholder='Description'
          value={description}
          style={{marginTop:10}}
          onChangeText={nextValue => setDescription(nextValue)}/>

        <SelectTags initialData={selectedTags} status={inputColor(selectedTags)} selectChange={setSelectedTags}></SelectTags>

        <Layout style={{flexDirection: 'row', marginTop: 10}}>

          <Button status='info' size='small' accessoryLeft={NavigationIcon} onPress={toggleLocalisation}>
          </Button>
          
            <Input
              status={inputColor(adresse)}
              placeholder='Adresse'
              value={adresse}
              accessoryLeft={PinIcon}
              disabled={usePosition}
              style={{marginLeft: 10, flex: 1}}
              onChangeText={nextValue => setAdresse(nextValue)}/>
        </Layout>
        
        <ButtonSave></ButtonSave>

      </Layout>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    favoris: state.lstFav
  }
}

export default connect(mapStateToProps)(AddPlace);