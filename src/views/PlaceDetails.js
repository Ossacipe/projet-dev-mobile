import React, {useState, useEffect} from 'react';
import { SafeAreaView, StyleSheet, ScrollView } from 'react-native';
import { Text, Layout, TopNavigation, Divider, TopNavigationAction, Button} from '@ui-kitten/components';
import { connect } from 'react-redux';
import { CloseIcon, BackIcon, EditIcon, PinIcon, TagIcon, CalendarIcon, PhoneCallIcon, LinkIcon, StarIcon, BookIcon, DownloadIcon } from '../definitions/Icones';
import Toast from 'react-native-toast-message';
import { displayPlaceType } from '../utils/Utils';
import { getPlaceDetails } from '../services/Google';

const PlaceDetails = ({ navigation, route, dispatch, favoris }) => {

  const [place, setPlace] = useState(favoris.find(val => val.addDate == route.params.datePlace));

  // Si la liste de favoris change, on recharge le lieu
  useEffect(() => {
    var newPlace = favoris.find(val => val.addDate == route.params.datePlace);
    if(newPlace){
      setPlace(newPlace);
    }
  }, [favoris]);

  // Retour à la page précédente
  const navigateBack = () => {
    navigation.goBack();
  };

  // Aller sur la page d'ajout / édition
  const navigateEdit = () => {
    navigation.navigate('ViewAddPlace', {place : place});
  };

  // Reload des data
  const reload = async () => {
    try {

      // Récupération du lieu détaillé
      var googlePlace = await getPlaceDetails(place.place_id);

      // Copie des nouvelles infos dans notre ancien objet
      var newPlace = Object.assign(place,googlePlace.result);

      // Update
      var action = { type: 'UPDATE', value: newPlace};
      dispatch(action);

      Toast.show({
        type: 'success',
        text1: 'Lieu actualisé !',
      });
    }
    catch(error){
      Toast.show({
        type: 'error',
        text1: error,
      });
    }
  };

  // Bouton retour
  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack}/>
  );

  // Bouton édition ou reload
  const EditAction = () => {
    if(place.place_id){
      return (<TopNavigationAction icon={DownloadIcon} onPress={reload}/>);
    }
    else {
      return (<TopNavigationAction icon={EditIcon} onPress={navigateEdit}/>);
    }
  }

  // Suppression
  const remove = () => {
    try {
      
      const action = { type: 'DELETE', value: place.addDate};
      dispatch(action);
      navigation.goBack();

      Toast.show({
        type: 'success',
        text1: 'Lieu correctement supprimé !',
      });
    }
    catch(error){
      Toast.show({
        type: 'error',
        text1: error,
      });
    }
  }

  // Affichage de la date
  const printDate = () => {
    var formattedDate = new Date(place.addDate).toLocaleString();
    return "Ajouté le " + formattedDate;
  }

  // Affichage de la description
  const PrintDescription = () => {
    if(place.description){
      return (<Layout><Text style={{padding: 20}}>{place.description}</Text><Divider/></Layout>);
    }
    else {
      return null;
    }
  }

  const PrintGoogleInfos = () => {
    if(place.place_id){
      return (
        <Layout style={{flex: 1}}>
          <Layout style={styles.blocInfo}>
            <PhoneCallIcon fill='#000000' style={styles.icon}></PhoneCallIcon>
            <Text style={{marginStart: 15}}>{place.international_phone_number}</Text>
          </Layout>
          <Divider/>
          <Layout style={styles.blocInfo}>
            <LinkIcon fill='#000000' style={styles.icon}></LinkIcon>
            <Text style={{marginStart: 15}}>{place.website}</Text>
          </Layout>
          <Divider/>
          <Layout style={styles.blocInfo}>
            <StarIcon fill='#000000' style={styles.icon}></StarIcon>
            <Text style={{marginStart: 15}}>{place.rating}</Text>
          </Layout>
          <Divider/>
          <Layout style={styles.blocInfo}>
            <BookIcon fill='#000000' style={styles.icon}></BookIcon>
            <Layout style={{marginStart: 15}}>
              { place.opening_hours && place.opening_hours.weekday_text ? place.opening_hours.weekday_text.map((item, index) => (<Text key={index}>{item}</Text>)) : null}
            </Layout>
          </Layout>
          <Divider/>
        </Layout>);
    }
    else {
      return null;
    }
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      <TopNavigation title={place.name} alignment='center' accessoryLeft={BackAction} accessoryRight={EditAction}/>
      <Divider/>
      <ScrollView>
        
        <PrintDescription/>

        <Layout style={styles.blocInfo}>
          <TagIcon fill='#000000' style={styles.icon}></TagIcon>
          <Text style={{marginStart: 15}}>{displayPlaceType(place.types)}</Text>
        </Layout>

        <Divider/>

        <Layout style={styles.blocInfo}>
          <PinIcon fill='#000000' style={styles.icon}></PinIcon>
          <Layout style={{marginStart: 15}}>
            <Text>{place.adress.name}</Text>
            <Text>{place.adress.postalCode} {place.adress.city}</Text>
          </Layout>
        </Layout>

        <Divider/>

        <Layout style={styles.blocInfo}>
          <CalendarIcon fill='#000000' style={styles.icon}></CalendarIcon>
          <Text style={{marginStart: 15}}>{printDate()}</Text>
        </Layout>

        <Divider/>

        <PrintGoogleInfos/>

        <Button style={{margin: 25}} status='danger' accessoryLeft={CloseIcon} onPress={remove}>
          Supprimer le lieu
        </Button>

      </ScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    favoris: state.lstFav
  }
}

export default connect(mapStateToProps)(PlaceDetails);

const styles = StyleSheet.create({
  blocInfo: {
    flex: 1,
    flexDirection: 'row', 
    padding: 20, 
    alignItems: 'center'
  },
  icon: {
    width: 30, 
    height: 30
  }
});