import React, {useEffect} from 'react';
import { SafeAreaView } from 'react-native';
import { Divider, Layout, TopNavigation, Input, Button, Select, SelectItem, IndexPath } from '@ui-kitten/components';
import { connect } from 'react-redux';
import PlaceList from '../components/PlaceList';
import { SearchIcon, PinIcon, NavigationIcon, CalendarIcon } from '../definitions/Icones';
import SelectTags from '../components/SelectTags';
import Toast from 'react-native-toast-message';
import { geocoding, getCurrentLocation, isPointInRadius, distanceBeteween2Points } from '../utils/Utils';

const ALL = "Toutes";

const Search = ({ navigation, favoris }) => {

  const [lstVille, setLstVille] = React.useState([...new Set([...[ALL], ...favoris.map(place => place.adress.city)])]); // Liste des villes présentes (distinct)
  const [lstPlace, setLstPlace] = React.useState([]); // Liste des lieux affichés
  const [nom, setNom] = React.useState(''); // Le nom de reherche
  const [rayon, setRayon] = React.useState(""); // La rayon de recherche
  const [selectedTags, setSelectedTags] = React.useState([]); // Les tags sélectionnés
  const [usePosition, setUsePosition] = React.useState(false); // Est-ce qu'on utilise la géolocalisation ?
  const [selectedVille, setSelectedVille] = React.useState(new IndexPath(0)); // Ville sélectionnée
  const [tri, setTri] = React.useState(true); // Tri par date ou par position ? (par défaut tri par position)
  const [selectedPosition, setSelectedPosition] = React.useState(null); // Position sélectionnée

  // Navigation vers les détails d'un lieu
  const navigateToPlaceDetails = (place) => {
    navigation.navigate("ViewPlaceDetails", { datePlace: place.addDate });
  };

  // Fonction pour activer / désactiver le mode localisation
  const toggleLocalisation = () => {
    Toast.show({
      type: 'info',
      text1: usePosition ? 'Mode position actuelle désactivé.' : 'L\'adresse utilisée sera votre position actuelle.',
    });
    setUsePosition(!usePosition);
  }

  // Obtient la position
  const getPosition = async () => {
    if(usePosition || displayVille == ALL){
      return getCurrentLocation();
    }
    else {
      let res = await geocoding(displayVille);
      return res[0];
    }
  }

  // Si la liste de favoris change, on recharge le lieu
  useEffect(() => {
    setLstVille([...new Set([...[ALL], ...favoris.map(place => place.adress.city)])]);
    setSelectedVille(new IndexPath(0));
    recherche();
  }, [favoris]);

  // Recherche
  const recherche = async () => {

    var newLst = [...favoris];

    try {
      
      var position = await getPosition();
      setSelectedPosition(position);

      // Filtrage sur le nom
      if(nom.length){
        newLst = newLst.filter(place => place.name.toLowerCase().includes(nom.toLowerCase()));
      }

      // Filtrage sur les tags
      if(selectedTags.length){
        newLst = newLst.filter(place => place.types.some(tag => selectedTags.includes(tag)));
      }

      // Filtrage sur la position
      if((displayVille != ALL || usePosition) && rayon.length){
        newLst = newLst.filter(place => isPointInRadius(place.position, position, rayon * 1000));
      }

      // Tri de la liste par distance
      newLst.sort((a,b) => distanceBeteween2Points(position,a.position) - distanceBeteween2Points(position,b.position));
      setTri(true);

      setLstPlace(newLst);
    }
    catch(error){
      Toast.show({
        type: 'error',
        text1: error,
      });
   }
  }

  // Affiche la ville actuellement sélectionnée
  const displayVille = lstVille[selectedVille.row];

  // Rendu des ville dans le select
  const renderOption = (title, index) => (<SelectItem key={index} title={title}/>);

  // Génération du bouton de tri
  const ButtonTri = () => {

    // Tri par distance (actuellement) donc proposition de tri par date
    if(tri){
      return (
        <Button appearance='ghost' accessoryLeft={PinIcon} onPress={triParDate}>
          Le plus proche
        </Button>);
    }

    // Tri par date d'ajout (actuellement) donc proposition de tri par distance
    else {
      return (
        <Button appearance='ghost' accessoryLeft={CalendarIcon} onPress={triParDistance}>
          Le plus récent
        </Button>);
    }
  }

  // Tri par distance
  const triParDistance = async() => {
    setTri(true);
    var sortingList = lstPlace.sort((a,b) => distanceBeteween2Points(selectedPosition,a.position) - distanceBeteween2Points(selectedPosition,b.position));
    setLstPlace(sortingList);
  }

  // Tri par date
  const triParDate = () => {
    setTri(false);
    var sortingList = lstPlace.sort((a,b) => -(new Date(a.addDate) - new Date(b.addDate)));
    setLstPlace(sortingList);
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title='Recherche' alignment='center'/>
      <Divider/>
      <Layout style={{ padding: 25}}>
        <Input
          placeholder='Nom'
          value={nom}
          onChangeText={nextValue => setNom(nextValue)}
          accessoryLeft={SearchIcon}/>

        <SelectTags selectChange={setSelectedTags}></SelectTags>

        <Layout style={{flexDirection: 'row', marginTop: 10}}>

          <Button status='info' size='small' accessoryLeft={NavigationIcon} onPress={toggleLocalisation}>
          </Button>

          <Select
            style={{ flex: 5, marginLeft: 10}}
            placeholder='Ville'
            value={displayVille}
            accessoryLeft={PinIcon}
            selectedIndex={selectedVille}
            disabled={usePosition}
            onSelect={index => setSelectedVille(index)}>
              {lstVille.map(renderOption)}
            </Select>

          <Input
            placeholder='Rayon'
            value={rayon}
            style={{ flex: 2, marginLeft: 10}}
            onChangeText={nextValue => setRayon(nextValue)}/>

        </Layout>

        <Button status='primary' accessoryLeft={SearchIcon} style={{marginTop:10}} onPress={recherche}>
          Recherche
        </Button>

      </Layout>
      <Divider/>
      <ButtonTri></ButtonTri>
      <Layout style={{ flex: 1, justifyContent: 'center' }}>

        <PlaceList onClickOnPlace={navigateToPlaceDetails} lstPlace={lstPlace}></PlaceList>
      </Layout>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    favoris: state.lstFav
  }
}

export default connect(mapStateToProps)(Search);