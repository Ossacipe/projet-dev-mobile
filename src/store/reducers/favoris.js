const initialState = { lstFav: [] }

function favoris(state = initialState, action) {
  let nextState
  switch (action.type) {

    // Ajouter un élément
    case 'ADD':
      nextState = {
        ...state,
        lstFav: [...state.lstFav, action.value]
      };
      return nextState || state
    
    // Mettre à jour un élément (en se basant sur la date d'ajout en tant qu'ID)
    case 'UPDATE':
      const index = state.lstFav.findIndex(place => place.addDate == action.value.addDate);
      const newArray = [...state.lstFav];
      newArray[index] = action.value;
      nextState = {
         ...state,
        lstFav: newArray
      };
      return nextState || state

    // Supprimer un élément
    case 'DELETE':
      nextState = {
        ...state,
        lstFav: state.lstFav.filter(place => place.addDate !== action.value)
      };
      return nextState || state
    
    default:
      return state
  }
}

export default favoris;