import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

import favorisReducer from './reducers/favoris';

const configPersist = {
  key: 'root',
  storage: AsyncStorage,
};

const reducerPersist = persistReducer(configPersist, favorisReducer);

export const Store = createStore(reducerPersist);
export const Persistor = persistStore(Store);