const API_KEY = 'AIzaSyBrFUEOiGOu-YDlP-iH03Qj6keML9VD33g';

export async function findPlace(adresse,type) {
  var url = `https://maps.googleapis.com/maps/api/place/textsearch/json?key=${API_KEY}&language=fr`

  if(adresse){
    url = url + `&query=${adresse}`;
  }

  if(type){
    url = url + `&type=${type}`;
  }
  
  const response = await fetch(url);
  return response.json();
}

export async function getPlaceDetails(id) {
  const url = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${id}&key=${API_KEY}&language=fr`
  const response = await fetch(url);
  return response.json();
}